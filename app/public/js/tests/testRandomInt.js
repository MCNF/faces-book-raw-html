'use strict';

module.exports = testRandomInt;

const randomInt = require('./../randomInt.js');
const assert = require('assert');
let a=0;
let chk=0;
let count= [0,0,0,0,0];
function testRandomInt() {
  test_randomInt_with_lower_bound_inclusive_and_out_bound_exclusive();
}

function test_randomInt_with_lower_bound_inclusive_and_out_bound_exclusive() {
  for(let i=0;i<1000;i++){
    a =randomInt(0,5);
    count[a]++;
  }

  if( (count[0]>=1)&&
      (count[1]>=1)&&
      (count[2]>=1)&&
      (count[3]>=1)&&
      (count[4]>=1)) {
      chk=1;
  }
  else {
      chk = 0;
  }

  assert.equal(1,chk);
}

testRandomInt();

const greenTrafficLightPattern = function() {
  return 'All tests passed';
};
console.log(greenTrafficLightPattern());
